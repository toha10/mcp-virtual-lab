classes:
- system.salt.minion.cert.proxy
- system.linux.system.repo.mcp.apt_mirantis.ubuntu
- system.linux.system.repo.mcp.apt_mirantis.glusterfs
- system.linux.system.repo.mcp.openstack
- system.memcached.server.single
- system.apache.server.single
- system.apache.server.site.gnocchi
- system.apache.server.site.panko
- system.glusterfs.server.cluster
- system.glusterfs.client.cluster
- system.glusterfs.client.volume.gnocchi
- system.glusterfs.server.volume.gnocchi
- service.redis.server.single
- system.nginx.server.single
- system.nginx.server.proxy.openstack.aodh
- system.apache.server.ssl
- system.nginx.server.proxy.ssl
- system.gnocchi.server.cluster
- system.gnocchi.common.storage.incoming.redis
- system.gnocchi.common.storage.file
- system.gnocchi.common.coordination.redis
- system.ceilometer.server.telemetry.cluster
- system.ceilometer.server.coordination.redis
- system.aodh.server.cluster
- system.aodh.server.coordination.redis
- system.panko.server.cluster
- system.ceilometer.server.backend.gnocchi
- cluster.virtual-mcp-pike-dvr-ssl
parameters:
  _param:
    salt_minion_ca_authority: salt_master_ca
    keepalived_openstack_telemetry_vip_address: ${_param:openstack_telemetry_address}
    keepalived_openstack_telemetry_vip_password: ${_param:openstack_telemetry_keepalived_password}
    keepalived_openstack_telemetry_vip_interface: ens4
    cluster_vip_address: ${_param:openstack_telemetry_address}
    cluster_local_address: ${_param:single_address}
    nginx_proxy_openstack_api_host: ${_param:openstack_telemetry_address}
    nginx_proxy_openstack_api_address: ${_param:cluster_local_address}
    nginx_proxy_openstack_aodh_host: 127.0.0.1
    nginx_proxy_ssl:
      authority: "${_param:salt_minion_ca_authority}"
      key_file: "/etc/ssl/private/internal_proxy.key"
      cert_file: "/etc/ssl/certs/internal_proxy.crt"
      chain_file: "/etc/ssl/certs/internal_proxy-with-chain.crt"
    apache_ssl:
      authority: "${_param:salt_minion_ca_authority}"
      key_file: "/etc/ssl/private/internal_proxy.key"
      cert_file: "/etc/ssl/certs/internal_proxy.crt"
      chain_file: "/etc/ssl/certs/internal_proxy-with-chain.crt"
    apache_gnocchi_api_address: ${_param:single_address}
    apache_panko_api_address: ${_param:single_address}
    cluster_node01_hostname: ${_param:openstack_telemetry_node01_hostname}
    cluster_node01_address: ${_param:openstack_telemetry_node01_address}
    cluster_node02_hostname: ${_param:openstack_telemetry_node02_hostname}
    cluster_node02_address: ${_param:openstack_telemetry_node02_address}
    cluster_node03_hostname: ${_param:openstack_telemetry_node03_hostname}
    cluster_node03_address: ${_param:openstack_telemetry_node03_address}
    glusterfs_service_host: ${_param:openstack_telemetry_address}
    gnocchi_glusterfs_service_host: ${_param:glusterfs_service_host}
    redis_sentinel_node01_address: ${_param:openstack_telemetry_node01_address}
    redis_sentinel_node02_address: ${_param:openstack_telemetry_node02_address}
    redis_sentinel_node03_address: ${_param:openstack_telemetry_node03_address}
    openstack_telemetry_redis_url: redis://${_param:redis_sentinel_node01_address}:26379?sentinel=master_1&sentinel_fallback=${_param:redis_sentinel_node02_address}:26379&sentinel_fallback=${_param:redis_sentinel_node03_address}:26379
    gnocchi_coordination_url: ${_param:openstack_telemetry_redis_url}
    gnocchi_storage_incoming_redis_url: ${_param:openstack_telemetry_redis_url}
    glusterfs_node01_address: ${_param:cluster_node01_address}
    glusterfs_node02_address: ${_param:cluster_node02_address}
    glusterfs_node03_address: ${_param:cluster_node03_address}
  linux:
    network:
      interface:
        ens4:
          enabled: true
          type: eth
          proto: static
          address: ${_param:single_address}
          netmask: 255.255.255.0
  salt:
    minion:
      cert:
        internal_proxy:
          host: ${_param:salt_minion_ca_host}
          authority: ${_param:salt_minion_ca_authority}
          common_name: internal_proxy
          signing_policy: cert_open
          alternative_names: IP:127.0.0.1,IP:${_param:cluster_local_address},IP:${_param:openstack_telemetry_address},DNS:${linux:system:name},DNS:${linux:network:fqdn},DNS:${_param:cluster_local_address},DNS:${_param:openstack_telemetry_address}
          key_file: "/etc/ssl/private/internal_proxy.key"
          cert_file: "/etc/ssl/certs/internal_proxy.crt"
          all_file: "/etc/ssl/certs/internal_proxy-with-chain.crt"
  keepalived:
    cluster:
      instance:
        VIP:
          virtual_router_id: 160
  redis:
    server:
      version: 3.0
      bind:
        address: ${_param:single_address}
    cluster:
      enabled: True
      mode: sentinel
      role: ${_param:redis_cluster_role}
      quorum: 2
      master:
        host: ${_param:cluster_node01_address}
        port: 6379
      sentinel:
        address: ${_param:single_address}
  apache:
    server:
      modules:
        - wsgi
  gnocchi:
    common:
      database:
        host: ${_param:openstack_database_address}
        ssl:
          enabled: ${_param:galera_ssl_enabled}
    server:
      identity:
        protocol: https
      pkgs:
      # TODO: move python-memcache installation to formula
      - gnocchi-api
      - gnocchi-metricd
      - python-memcache
  panko:
    server:
      identity:
        protocol: https
      database:
        ssl:
          enabled: ${_param:galera_ssl_enabled}
  aodh:
    server:
      bind:
        host: 127.0.0.1
      coordination_backend:
        url: ${_param:openstack_telemetry_redis_url}
      identity:
        protocol: https
        host: ${_param:openstack_control_address}
      database:
        ssl:
          enabled: ${_param:galera_ssl_enabled}
      message_queue:
        port: ${_param:rabbitmq_port}
        ssl:
          enabled: ${_param:rabbitmq_ssl_enabled}
  ceilometer:
    server:
      bind:
        host: 127.0.0.1
      coordination_backend:
        url: ${_param:openstack_telemetry_redis_url}
      identity:
        protocol: https
        host: ${_param:openstack_control_address}
      message_queue:
        port: ${_param:rabbitmq_port}
        ssl:
          enabled: ${_param:rabbitmq_ssl_enabled}
  haproxy:
    proxy:
      listen:
        panko_api:
          type: ~
        gnocchi_api:
          type: ~
        aodh-api:
          type: ~
