classes:
- service.git.client
- system.linux.system.single
- system.linux.system.single.debian
- system.linux.system.repo.mcp.apt_mirantis.saltstack
- system.linux.system.repo.mcp.apt_mirantis.openstack
- system.openssh.client.lab
- system.salt.master.pkg
- system.salt.master.api
- system.salt.master.formula.pkg.manila
- system.salt.master.formula.pkg.gnocchi
- system.salt.master.formula.pkg.panko
# Add when reclass bumped
#-salt/master/formula/pkg/dogtag.yml
- system.reclass.storage.salt
- system.salt.minion.ca.salt_master
- system.keystone.client.single
- system.keystone.client.service.nova21
- system.keystone.client.service.nova-placement
- system.keystone.client.service.cinder3
- system.keystone.client.service.designate
- system.keystone.client.service.manila
- system.keystone.client.service.manila2
- system.keystone.client.service.aodh
- system.keystone.client.service.ceilometer
- system.keystone.client.service.panko
- system.keystone.client.service.gnocchi
- system.keystone.client.service.barbican
- system.gnocchi.client
- system.gnocchi.client.v1.archive_policy.default
- system.manila.client
- system.reclass.storage.system.openstack_dns_cluster
- system.reclass.storage.system.openstack_control_cluster
- system.reclass.storage.system.openstack_compute_multi
- system.reclass.storage.system.openstack_gateway_single
- system.reclass.storage.system.openstack_dashboard_single
- system.reclass.storage.system.openstack_share_single
- system.reclass.storage.system.openstack_telemetry_cluster
- cluster.virtual-mcp-pike-dvr-ssl-barbican
parameters:
  _param:
    reclass_data_repository: https://gerrit.mcp.mirantis.net/salt-models/mcp-virtual-lab
    reclass_data_revision: master
    salt_master_environment_repository: "https://github.com/tcpcloud"
    salt_master_environment_revision: master
    salt_api_password_hash: "$6$sGnRlxGf$al5jMCetLP.vfI/fTl3Z0N7Za1aeiexL487jAtyRABVfT3NlwZxQGVhO7S1N8OwS/34VHYwZQA8lkXwKMN/GS1"
    reclass_config_master: 192.168.10.90
    single_address: 172.16.10.100
    salt_master_host: 127.0.0.1
    salt_master_base_environment: prd
    manila_share_type_default_extra_specs:
      driver_handles_share_servers: False
      snapshot_support: True
      create_share_from_snapshot_support : True
      mount_snapshot_support : True
      revert_to_snapshot_support : True
  linux:
    network:
      interface:
        ens4:
          enabled: true
          type: eth
          proto: static
          address: ${_param:single_address}
          netmask: 255.255.255.0
  salt:
    master:
      reactor:
        reclass/minion/classify:
        - salt://reclass/reactor/node_register.sls
      environment:
        prd:
          formula:
            dogtag:
              source: pkg
              name: salt-formula-dogtag
  reclass:
    storage:
      class_mapping:
        common_node:
          expression: all
          node_param:
            single_address:
              value_template: <<node_control_ip>>
            linux_system_codename:
              value_template: <<node_os>>
            salt_master_host:
              value_template: <<node_master_ip>>
        infra_config:
          expression: <<node_hostname>>__startswith__cfg
          cluster_param:
            infra_config_address:
              value_template: <<node_control_ip>>
            infra_config_deploy_address:
              value_template: <<node_deploy_ip>>
            cluster_domain:
              value_template: <<node_domain>>
        openstack_control01:
          expression: <<node_hostname>>__equals__ctl01
          cluster_param:
            openstack_control_node01_address:
              value_template: <<node_control_ip>>
        openstack_control02:
          expression: <<node_hostname>>__equals__ctl02
          cluster_param:
            openstack_control_node02_address:
              value_template: <<node_control_ip>>
        openstack_control03:
          expression: <<node_hostname>>__equals__ctl03
          cluster_param:
            openstack_control_node03_address:
              value_template: <<node_control_ip>>
        openstack_compute:
          expression: <<node_hostname>>__startswith__cmp
          node_class:
            value_template:
              - cluster.<<node_cluster>>.openstack.compute
          node_param:
            tenant_address:
              value_template: <<node_tenant_ip>>
            external_address:
              value_template: <<node_external_ip>>
        openstack_dns01:
          expression: <<node_hostname>>__equals__dns01
          cluster_param:
            openstack_dns_node01_address:
              value_template: <<node_control_ip>>
        openstack_dns02:
          expression: <<node_hostname>>__equals__dns02
          cluster_param:
            openstack_dns_node02_address:
              value_template: <<node_control_ip>>
        openstack_gateway:
          expression: <<node_hostname>>__startswith__gtw
          node_class:
            value_template:
              - cluster.<<node_cluster>>.openstack.gateway
          node_param:
            tenant_address:
              value_template: <<node_tenant_ip>>
            external_address:
              value_template: <<node_external_ip>>
          cluster_param:
            openstack_gateway_address:
              value_template: <<node_control_ip>>
            openstack_gateway_node01_external_address:
              value_template: <<node_external_ip>>
        openstack_dashboard:
          expression: <<node_hostname>>__startswith__prx
          node_class:
            value_template:
              - cluster.<<node_cluster>>.openstack.dashboard
              - cluster.<<node_cluster>>.openstack.proxy
          cluster_param:
            openstack_proxy_node01_address:
              value_template: <<node_control_ip>>
        openstack_share:
          expression: <<node_hostname>>__startswith__share
          node_class:
            value_template:
              - cluster.<<node_cluster>>.openstack.share
          cluster_param:
            openstack_share_node01_address:
              value_template: <<node_control_ip>>
            openstack_share_node01_share_address:
              value_template: <<node_tenant_ip>>
        openstack_telemetry01:
          expression: <<node_hostname>>__equals__mdb01
          cluster_param:
            openstack_telemetry_node01_address:
              value_template: <<node_control_ip>>
        openstack_telemetry02:
          expression: <<node_hostname>>__equals__mdb02
          cluster_param:
            openstack_telemetry_node02_address:
              value_template: <<node_control_ip>>
        openstack_telemetry03:
          expression: <<node_hostname>>__equals__mdb03
          cluster_param:
            openstack_telemetry_node03_address:
              value_template: <<node_control_ip>>
      node:
        openstack_control_node01:
          classes:
          - service.galera.master.cluster
          - service.dogtag.server.cluster.master
          params:
            mysql_cluster_role: master
            linux_system_codename: xenial
        openstack_control_node02:
          classes:
          - service.galera.slave.cluster
          - service.dogtag.server.cluster.slave
          params:
            mysql_cluster_role: slave
            linux_system_codename: xenial
        openstack_control_node03:
          classes:
          - service.galera.slave.cluster
          - service.dogtag.server.cluster.slave
          params:
            mysql_cluster_role: slave
            linux_system_codename: xenial
        openstack_compute_node01:
          params:
            single_address: 172.16.10.105
            tenant_address: 10.1.0.105
            external_address: 10.16.0.105
        openstack_compute_node02:
          params:
            single_address: 172.16.10.106
            tenant_address: 10.1.0.106
            external_address: 10.16.0.106
        openstack_gateway_node01:
          params:
            tenant_address: 10.1.0.110
            external_address: ${_param:openstack_gateway_node01_external_address}
        openstack_share_node01:
          params:
            single_address: 172.16.10.130
            tenant_address: 10.1.0.130
        openstack_proxy_node01:
          classes:
          - cluster.virtual-mcp-pike-dvr-ssl-barbican.openstack.proxy
          params:
            linux_system_codename: xenial
        openstack_telemetry_node01:
          params:
            # create resources only from 1 controller
            # to prevent race conditions
            ceilometer_create_gnocchi_resources: true
            redis_cluster_role: 'master'
        openstack_telemetry_node02:
          params:
            redis_cluster_role: 'slave'
        openstack_telemetry_node03:
          params:
            redis_cluster_role: 'slave'
