classes:
- system.salt.minion.cert.proxy
- system.salt.minion.cert.mysql.server
- system.salt.minion.cert.rabbitmq_server
- system.linux.system.lowmem
- system.linux.system.repo.mcp.apt_mirantis.glusterfs
- system.linux.system.repo.mcp.apt_mirantis.openstack
- system.linux.system.repo.mcp.extra
- system.linux.system.repo.mcp.apt_mirantis.saltstack
- system.memcached.server.single
- system.rabbitmq.server.cluster
- service.rabbitmq.server.ssl
- system.rabbitmq.server.vhost.openstack
- system.apache.server.site.manila
- system.apache.server.site.barbican
- system.apache.server.site.nova-placement
- system.apache.server.site.cinder
- system.nginx.server.single
- system.nginx.server.proxy.openstack_api
- system.nginx.server.proxy.openstack.designate
- system.nginx.server.proxy.openstack.glance_registry
- system.keystone.server.wsgi
- system.keystone.server.cluster
- system.glusterfs.client.cluster
- system.glusterfs.client.volume.glance
- system.glusterfs.client.volume.keystone
- system.glusterfs.server.volume.glance
- system.glusterfs.server.volume.keystone
- system.glusterfs.server.cluster
- system.glance.control.cluster
- system.nova.control.cluster
- system.neutron.control.openvswitch.cluster
- system.cinder.control.cluster
- system.cinder.control.backend.lvm
- system.heat.server.cluster
- system.designate.server.cluster
- system.galera.server.cluster
- service.galera.ssl
- system.apache.server.ssl
- system.nginx.server.proxy.ssl
- system.galera.server.database.cinder
- system.galera.server.database.glance
- system.galera.server.database.heat
- system.galera.server.database.keystone
- system.galera.server.database.nova
- system.galera.server.database.designate
- system.galera.server.database.manila
- system.galera.server.database.aodh
- system.galera.server.database.panko
- system.galera.server.database.gnocchi
- system.galera.server.database.barbican
- system.dogtag.server.cluster
- system.barbican.server.cluster
- service.barbican.server.plugin.dogtag
- system.ceilometer.client
- system.ceilometer.client.cinder_volume
- system.ceilometer.client.neutron
- system.haproxy.proxy.listen.openstack.placement
- system.haproxy.proxy.listen.openstack.manila
- system.manila.control.cluster
- cluster.virtual-mcp-pike-dvr-ssl-barbican
parameters:
  _param:
    keepalived_vip_interface: ens4
    salt_minion_ca_authority: salt_master_ca
    ### nginx ssl sites settings
    nginx_proxy_ssl:
      authority: "${_param:salt_minion_ca_authority}"
      key_file: "/etc/ssl/private/internal_proxy.key"
      cert_file: "/etc/ssl/certs/internal_proxy.crt"
      chain_file: "/etc/ssl/certs/internal_proxy-with-chain.crt"
    apache_ssl:
      authority: "${_param:salt_minion_ca_authority}"
      key_file: "/etc/ssl/private/internal_proxy.key"
      cert_file: "/etc/ssl/certs/internal_proxy.crt"
      chain_file: "/etc/ssl/certs/internal_proxy-with-chain.crt"
    nginx_proxy_openstack_api_address: ${_param:cluster_local_address}
    nginx_proxy_openstack_keystone_host: 127.0.0.1
    nginx_proxy_openstack_nova_host: 127.0.0.1
    nginx_proxy_openstack_glance_host: 127.0.0.1
    nginx_proxy_openstack_neutron_host: 127.0.0.1
    nginx_proxy_openstack_heat_host: 127.0.0.1
    nginx_proxy_openstack_designate_host: 127.0.0.1
    apache_manila_api_address: ${_param:single_address}
    apache_keystone_api_host: ${_param:single_address}
    apache_barbican_api_address: ${_param:cluster_local_address}
    apache_barbican_api_host: ${_param:single_address}
    apache_nova_placement_api_address: ${_param:cluster_local_address}
    barbican_dogtag_nss_password: workshop
    barbican_dogtag_host: ${_param:cluster_vip_address}
    apache_cinder_api_address: ${_param:cluster_local_address}
    # dogtag listens on 8443 but there is no way to bind it to
    # Specific IP, as on this setup dogtag installed on ctl nodes
    # Change port on haproxy side to avoid binding conflict.
    haproxy_dogtag_bind_port: 8444
    cluster_dogtag_port: 8443
    dogtag_master_host: ctl01.${linux:system:domain}
    dogtag_pki_admin_password: workshop
    dogtag_pki_client_database_password: workshop
    dogtag_pki_client_pkcs12_password: workshop
    dogtag_pki_ds_password: workshop
    dogtag_pki_token_password: workshop
    dogtag_pki_security_domain_password: workshop
    dogtag_pki_clone_pkcs12_password: workshop
  rabbitmq:
    server:
      ssl:
        enabled: ${_param:rabbitmq_ssl_enabled}
  nginx:
    server:
      site:
        nginx_proxy_openstack_api_keystone:
          enabled: false
        nginx_proxy_openstack_api_keystone_private:
          enabled: false
        nginx_proxy_openstack_api_cinder:
          enabled: false
  linux:
    system:
      package:
        python-msgpack:
          version: latest
    network:
      interface:
        ens4:
          enabled: true
          type: eth
          proto: static
          address: ${_param:single_address}
          netmask: 255.255.255.0
  keepalived:
    cluster:
      instance:
        VIP:
          virtual_router_id: 150
  dogtag:
   server:
     ldap_hostname: ${linux:network:fqdn}
     ldap_dn_password: workshop
     ldap_admin_password: workshop
     export_pem_file_path: /etc/dogtag/kra_admin_cert.pem
  # TODO drop this once reclass bumped, missing part in current version
  apache:
    server:
      site:
        barbican_admin:
          host:
            address: ${_param:apache_barbican_api_address}
            name: ${_param:apache_barbican_api_host}
            port: 9312
          log:
            custom:
              format: 'combined'
              file: '/var/log/barbican/barbican-api.log'
            error:
              enabled: true
              file: '/var/log/barbican/barbican-api.log'
  barbican:
    server:
      enabled: true
      dogtag_admin_cert:
        engine: mine
        minion: ${_param:dogtag_master_host}
      ks_notifications_enable: True
      store:
        software:
          store_plugin: dogtag_crypto
          global_default: True
      plugin:
        dogtag:
          port: ${_param:haproxy_dogtag_bind_port}
      database:
        ssl:
          enabled: ${_param:galera_ssl_enabled}
      message_queue:
        port: ${_param:rabbitmq_port}
        ssl:
          enabled: ${_param:rabbitmq_ssl_enabled}
  keystone:
    server:
      admin_email: ${_param:admin_email}
      database:
        ssl:
          enabled: ${_param:galera_ssl_enabled}
      message_queue:
        port: ${_param:rabbitmq_port}
        ssl:
          enabled: ${_param:rabbitmq_ssl_enabled}
  designate:
    pool_manager:
      enabled: ${_param:designate_pool_manager_enabled}
      periodic_sync_interval: ${_param:designate_pool_manager_periodic_sync_interval}
    server:
      identity:
        protocol: https
      bind:
        api:
          address: 127.0.0.1
      database:
        ssl:
          enabled: ${_param:galera_ssl_enabled}
      message_queue:
        port: ${_param:rabbitmq_port}
        ssl:
          enabled: ${_param:rabbitmq_ssl_enabled}
      backend:
        pdns4:
          api_token: ${_param:designate_pdns_api_key}
          api_endpoint: ${_param:designate_pdns_api_endpoint}
      mdns:
        address: ${_param:designate_mdns_address}
        port: ${_param:designate_mdns_port}
      pools:
        default:
          description: 'test pool'
          targets:
            default:
              description: 'test target1'
            default1:
              type: ${_param:designate_pool_target_type}
              description: 'test target2'
              masters: ${_param:designate_pool_target_masters}
              options:
                host: ${_param:openstack_dns_node02_address}
                port: 53
                api_endpoint: "http://${_param:openstack_dns_node02_address}:${_param:powerdns_webserver_port}"
                api_token: ${_param:designate_pdns_api_key}
      quota:
        zones: ${_param:designate_quota_zones}
  glance:
    server:
      barbican:
        enabled: ${_param:barbican_integration_enabled}
      storage:
        engine: file
      images: []
      workers: 1
      bind:
        address: 127.0.0.1
      identity:
        protocol: https
      registry:
        protocol: https
      database:
        ssl:
          enabled: ${_param:galera_ssl_enabled}
      message_queue:
        port: ${_param:rabbitmq_port}
        ssl:
          enabled: ${_param:rabbitmq_ssl_enabled}
  heat:
    server:
      bind:
        api:
          address: 127.0.0.1
        api_cfn:
          address: 127.0.0.1
        api_cloudwatch:
          address: 127.0.0.1
      identity:
        protocol: https
      database:
        ssl:
          enabled: ${_param:galera_ssl_enabled}
      message_queue:
        port: ${_param:rabbitmq_port}
        ssl:
          enabled: ${_param:rabbitmq_ssl_enabled}
      # Since we using self signed cert not present in images, we have to
      # use insecure option when sending signal to wait condition from instance.
      clients:
        heat:
          insecure: true
  neutron:
    server:
      bind:
        address: 127.0.0.1
      identity:
        protocol: https
      database:
        ssl:
          enabled: ${_param:galera_ssl_enabled}
      message_queue:
        port: ${_param:rabbitmq_port}
        ssl:
          enabled: ${_param:rabbitmq_ssl_enabled}
  nova:
    controller:
      networking: dvr
      cpu_allocation: 54
      barbican:
        enabled: ${_param:barbican_integration_enabled}
      metadata:
        password: ${_param:metadata_password}
        bind:
          address: ${_param:cluster_local_address}
      bind:
        public_address: ${_param:cluster_vip_address}
        novncproxy_port: 6080
        private_address: 127.0.0.1
      identity:
        protocol: https
      network:
        protocol: https
      glance:
        protocol: https
      vncproxy_url: http://${_param:cluster_vip_address}:6080
      workers: 1
      database:
        ssl:
          enabled: ${_param:galera_ssl_enabled}
      message_queue:
        port: ${_param:rabbitmq_port}
        ssl:
          enabled: ${_param:rabbitmq_ssl_enabled}
      notification:
        notify_on:
          state_change: vm_and_task_state
  cinder:
    controller:
      controller:
      barbican:
        enabled: ${_param:barbican_integration_enabled}
      identity:
        protocol: https
      osapi:
        host: 127.0.0.1
      glance:
        protocol: https
      database:
        ssl:
          enabled: ${_param:galera_ssl_enabled}
      message_queue:
        port: ${_param:rabbitmq_port}
        ssl:
          enabled: ${_param:rabbitmq_ssl_enabled}
  manila:
    common:
      identity:
        protocol: https
      default_share_type: default
      database:
        ssl:
          enabled: ${_param:galera_ssl_enabled}
      message_queue:
        port: ${_param:rabbitmq_port}
        ssl:
          enabled: ${_param:rabbitmq_ssl_enabled}
  salt:
    minion:
      cert:
        internal_proxy:
          host: ${_param:salt_minion_ca_host}
          authority: ${_param:salt_minion_ca_authority}
          common_name: internal_proxy
          signing_policy: cert_open
          alternative_names: IP:127.0.0.1,IP:${_param:cluster_local_address},IP:${_param:cluster_public_host},DNS:${linux:system:name},DNS:${linux:network:fqdn},DNS:${_param:cluster_local_address},DNS:${_param:cluster_public_host}
          key_file: "/etc/ssl/private/internal_proxy.key"
          cert_file: "/etc/ssl/certs/internal_proxy.crt"
          all_file: "/etc/ssl/certs/internal_proxy-with-chain.crt"
  haproxy:
    proxy:
      listen:
        barbican-api:
          type: ~
        barbican-admin-api:
          type: ~
        designate_api:
          type: ~
        keystone_public_api:
          type: ~
        keystone_admin_api:
          type: ~
        manila_api:
          type: ~
        nova_api:
          type: ~
        nova_metadata_api:
          type: ~
        cinder_api:
          type: ~
        glance_api:
          type: ~
        glance_registry_api:
          type: ~
        heat_cloudwatch_api:
          type: ~
        heat_api:
          type: ~
        heat_cfn_api:
          type: ~
        neutron_api:
          type: ~
        placement_api:
          type: ~
