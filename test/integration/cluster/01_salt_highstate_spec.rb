
describe command('salt-call pillar.items') do
  its('exit_status') { should eq 0 }
end

# to enforce full highstate
# once switched fully to new reclass
#describe command('salt-call state.apply') do
  #its('exit_status') { should eq 0 }
  ##its('stdout') { should match (/Failed: 0/) }
#end
